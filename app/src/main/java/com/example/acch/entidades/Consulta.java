package com.example.acch.entidades;

import java.util.Date;

public class Consulta extends Agenda {
    Pessoa pessoa;
    Medico medico;
    String motivo;

    public Consulta(Date dia, Date mes, Date ano, Date horario, String status, Pessoa pessoa, Medico medico, String motivo) {
        super(dia, mes, ano, horario, status);
        this.pessoa = pessoa;
        this.medico = medico;
        this.motivo = motivo;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
}
