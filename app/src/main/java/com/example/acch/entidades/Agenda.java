package com.example.acch.entidades;

import java.util.Date;

public class Agenda {
    private Date dia, mes, ano, horario;
    String status;

    public Agenda(Date dia, Date mes, Date ano, Date horario, String status) {
        this.dia = dia;
        this.mes = mes;
        this.ano = ano;
        this.horario = horario;
        this.status = status;
    }

    public Date getDia() {
        return dia;
    }

    public void setDia(Date dia) {
        this.dia = dia;
    }

    public Date getMes() {
        return mes;
    }

    public void setMes(Date mes) {
        this.mes = mes;
    }

    public Date getAno() {
        return ano;
    }

    public void setAno(Date ano) {
        this.ano = ano;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
