package com.example.acch.entidades;

import java.util.Date;

public class Pessoa {
    private String nome, rg, cpf, telefone, celular, senha;
    private Date dataNascto;

    public Pessoa(String nome, String rg, String cpf, String telefone, String celular,
                  Date dataNascto) {
        this.nome = nome;
        this.rg = rg;
        this.cpf = cpf;
        this.telefone = telefone;
        this.celular = celular;
        this.dataNascto = dataNascto;
    }

    public Pessoa(){

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Date getDataNascto() {
        return dataNascto;
    }

    public void setDataNascto(Date dataNascto) {
        this.dataNascto = dataNascto;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
