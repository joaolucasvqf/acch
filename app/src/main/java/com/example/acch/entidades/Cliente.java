package com.example.acch.entidades;

import java.io.Serializable;
import java.util.Date;

public class Cliente extends Pessoa implements Serializable {
    String planoDeSaude;

    public Cliente(String nome, String rg, String cpf, String telefone, String celular,
                   Date dataNascto, String planoDeSaude) {
        super();
        this.setNome(nome);
        this.setRg(rg);
        this.setCpf(cpf);
        this.setTelefone(telefone);
        this.setCelular(celular);
        this.setDataNascto(dataNascto);
        this.planoDeSaude = planoDeSaude;
    }

    public String getPlanoDeSaude() {
        return planoDeSaude;
    }

    public void setPlanoDeSaude(String planoDeSaude) {
        this.planoDeSaude = planoDeSaude;
    }
}
