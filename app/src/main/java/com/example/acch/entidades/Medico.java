package com.example.acch.entidades;

import android.location.Address;

import java.util.Date;

public class Medico extends Pessoa {
    private String crm, especialidade;
    private int nroConsultorio;

    public Medico(String nome, String rg, String cpf, String telefone, String celular,
                  Date dataNascto, String crm, String especialidade, int nroConsultorio) {
        super();
        this.setNome(nome);
        this.setRg(rg);
        this.setCpf(cpf);
        this.setTelefone(telefone);
        this.setCelular(celular);
        this.setDataNascto(dataNascto);
        this.crm = crm;
        this.especialidade = especialidade;
        this.nroConsultorio = nroConsultorio;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public String getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(String especialidade) {
        this.especialidade = especialidade;
    }

    public int getNroConsultorio() {
        return nroConsultorio;
    }

    public void setNroConsultorio(int nroConsultorio) {
        this.nroConsultorio = nroConsultorio;
    }
}
