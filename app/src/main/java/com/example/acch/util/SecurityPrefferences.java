package com.example.acch.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SecurityPrefferences {

    private final SharedPreferences mSharedPreferences;

    public SecurityPrefferences(Context context){
        this.mSharedPreferences = context.getSharedPreferences("app_acch", Context.MODE_PRIVATE);
    }

    public void storeString(String key, String value){
        this.mSharedPreferences.edit().putString(key, value).apply();
    }

    public String getStoreString(String key){
        return this.mSharedPreferences.getString(key, "");
    }
}
