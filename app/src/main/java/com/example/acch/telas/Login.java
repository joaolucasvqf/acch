package com.example.acch.telas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.acch.R;
import com.example.acch.telasAdm.MenuPrincipalAdm;
import com.example.acch.util.SecurityPrefferences;

public class Login extends Activity {

    Button btnLogin, btnSingUp;
    EditText txtUserName, txtPassword;
    TextView btnRecoverPassword;
    CheckBox savePassword;
    Intent intent;
    SecurityPrefferences prefferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        btnLogin = findViewById(R.id.btnLogin);
        btnSingUp = findViewById(R.id.btnSingUp);
        txtUserName = findViewById(R.id.inputUserName);
        txtPassword = findViewById(R.id.inputPassword);
        btnRecoverPassword = findViewById(R.id.btnRecoveryPassword);
        savePassword = findViewById(R.id.savePassword);

        prefferences = new SecurityPrefferences(this);

        if (!prefferences.getStoreString("password").isEmpty()) {
            savePassword.setChecked(true);
            txtUserName.setText(prefferences.getStoreString("username"));
            txtPassword.setText(prefferences.getStoreString("password"));
        } else {
            savePassword.setChecked(false);
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Fazer Login
                if (!txtUserName.getText().toString().equals("") && !txtPassword.getText().toString().equals("")) {
                    prefferences.storeString("username", txtUserName.getText().toString());
                    if (savePassword.isChecked()){
                        prefferences.storeString("login", txtUserName.getText().toString());
                        prefferences.storeString("password", txtPassword.getText().toString());
                    }
                    if (txtUserName.getText().toString().equals("1")){
                        intent = new Intent(Login.this, MenuPrincipal.class);
                    }else if (txtUserName.getText().toString().equals("2")){
                        intent = new Intent(Login.this, MenuPrincipalAdm.class);
                    }
                    intent = new Intent(Login.this, MenuPrincipal.class);

                    startActivity(intent);
                }else if (txtUserName.getText().toString().equals("")){
                    Toast.makeText(Login.this, "Insira um usuário", Toast.LENGTH_LONG).show();
                }else if(txtPassword.getText().toString().equals("")){
                    Toast.makeText(Login.this, "Insira uma senha", Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(Login.this, "Informações de login inválidas", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnSingUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Login.this, Cadastro.class);

                startActivity(intent);
            }
        });

        btnRecoverPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Login.this, "Fucionalidade ainda indisponível", Toast.LENGTH_LONG).show();
            }
        });

        savePassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked){
                    prefferences.storeString("password", null);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
