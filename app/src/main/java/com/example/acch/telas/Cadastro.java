package com.example.acch.telas;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.acch.R;
import com.example.acch.entidades.Cliente;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.example.acch.adapters.Mascaras.FORMAT_CALENDARIO;
import static com.example.acch.adapters.Mascaras.FORMAT_CELLULAR;
import static com.example.acch.adapters.Mascaras.FORMAT_CPF;
import static com.example.acch.adapters.Mascaras.FORMAT_FONE;
import static com.example.acch.adapters.Mascaras.FORMAT_RG;
import static com.example.acch.adapters.Mascaras.mask;

public class Cadastro extends Activity {

    final Calendar myCalendar = Calendar.getInstance();
    Cliente cliente;
    LinearLayout cadastro, senha;
    Button btnVoltar, limpar, proximo, concluir;
    EditText nome, rg, cpf, telefone, celular, dataNascimento, plano, senha1, senha2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        btnVoltar = findViewById(R.id.btnVoltar);
        limpar = findViewById(R.id.limpar);
        proximo = findViewById(R.id.proximo);
        concluir = findViewById(R.id.concluirCadastro);
        cadastro = findViewById(R.id.cadastro);
        senha = findViewById(R.id.senha);
        nome = findViewById(R.id.nome);
        rg = findViewById(R.id.rg);
        cpf = findViewById(R.id.cpf);
        telefone = findViewById(R.id.telefone);
        celular = findViewById(R.id.celular);
        dataNascimento = findViewById(R.id.dataNascimento);
        plano = findViewById(R.id.plano);
        senha1 = findViewById(R.id.password);
        senha2 = findViewById(R.id.passwordConfirm);

        rg.addTextChangedListener(mask(rg, FORMAT_RG));
        cpf.addTextChangedListener(mask(cpf, FORMAT_CPF));
        telefone.addTextChangedListener(mask(telefone, FORMAT_FONE));
        celular.addTextChangedListener(mask(celular, FORMAT_CELLULAR));
        dataNascimento.addTextChangedListener(mask(dataNascimento, FORMAT_CALENDARIO));

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        dataNascimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(Cadastro.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        limpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nome = null;
                rg = null;
                cpf = null;
                telefone = null;
                celular = null;
                dataNascimento = null;
                plano = null;
            }
        });

        proximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    cliente = new Cliente(
                            nome.getText().toString(),
                            rg.getText().toString(),
                            cpf.getText().toString(),
                            telefone.getText().toString(),
                            celular.getText().toString(),
                            (Date) dataNascimento.getText(),
                            plano.getText().toString());
                    cadastro.setVisibility(View.INVISIBLE);
                    senha.setVisibility(View.VISIBLE);
                } catch (Exception e) {

                }
            }
        });

        concluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (senha1.getText() == senha2.getText()){
                    startActivity(new Intent(Cadastro.this, Login.class));
                }
            }
        });

    }

    public void onResume() {

        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);

        super.onResume();
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dataNascimento.setText(sdf.format(myCalendar.getTime()));
    }
}
