package com.example.acch.telas;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.acch.R;

public class Contato extends Activity {

    Button btnEnviar;
    ImageButton btnVoltar;
    EditText titulo, mensagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contato);

        btnEnviar = findViewById(R.id.btnEnviar);
        btnVoltar = findViewById(R.id.btnVoltar);
        titulo = findViewById(R.id.titulo);
        mensagem = findViewById(R.id.mensagem);

        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isTituloEmpty = titulo.getText().toString().equals("");
                boolean isMensagemEmpty = mensagem.getText().toString().equals("");

                if (isTituloEmpty) {
                    Toast.makeText(Contato.this, "Insira um título válido", Toast.LENGTH_LONG).show();
                } else if (isMensagemEmpty) {
                    Toast.makeText(Contato.this, "Insira uma mensagem válida", Toast.LENGTH_LONG).show();
                } else {
                    try {
                        //função enviar mensagem
                        Toast.makeText(Contato.this, "Mensagem enviada com sucesso", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(Contato.this, MenuPrincipal.class));
                    } catch (Exception e) {
                        Toast.makeText(Contato.this, "Erro ao enviar mensagem", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
